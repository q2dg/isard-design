## Configuración del servidor de desarrollo

Name: Development v4 

IP: http://192.168.129.125

Hasura: http://192.168.129.125:8080

Go GraphQL: http://192.168.129.125:1312


**Com actualitzar la versió de códi de la màquina:**

Entrar a la maquina "Development V4" en develop.isardvdi.com

1. $(whoami) == 'dev'
2. cd /data/dev/workspace/isardvdi
3. $(git branch) == 'v3-develop'
4. git pull
5. make docker
6. make docker-compose-up

**Script que esborra tot, crea les taules de nou i fa l'insert d'un usuari:**

run cli/cmd/cli/main.go

**Conexión a la maquina por vpn:**

- install wireguard
- sudo wg-quick up /etc/wireguard/{name}.conf

**Ejecutar file para datos DB:**

1- docker inspect isardvdi_postgres_1 i trobes la IPAddress

2- Canvies al fitxer cli/cmd/cli/main.go la ip a l'hora de connectar-se al postgres

3- go run cli/cmd/cli/main.go

**Visualizar logs de error:**

docker logs isardvdi_backend_1

