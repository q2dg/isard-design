# Documentation compilation for Isard systems design

This documentation is writed in Markdown using [MkDocs+Gitlab](https://gitlab.com/pages/mkdocs).

See `docs` directory for Markdown files or the [auto-built site](https://isard.gitlab.io/isard-design).
